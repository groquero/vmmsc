#include "hybrid_window.h"
#include "vmm_window.h"

HybridWindow::HybridWindow(FECWindow *top, unsigned short fec, unsigned short hybrid, QWidget *parent) :
    QWidget(parent),
    m_fecWindow{top},
    m_fecIndex{fec},
    m_hybridIndex{hybrid},
    m_ui(new Ui::hybrid_window)
{
    m_ui->setupUi(this);
    UpdateWindow();
    m_ui->tpSkew->clear();
    for(int i=0;i < 2; i++) {
        for(int n=0;n< 8; n++) {
            QString txt = QStringLiteral("%1 ns").arg((i*25)+n*25/8.0);
            m_ui->tpSkew->addItem(txt);
        }
    }
    LoadSettings();

    connect(m_ui->axis, SIGNAL(currentIndexChanged(int)),
            this, SLOT(onUpdateSettings()));
    connect(m_ui->position, SIGNAL(valueChanged(int)),
            this, SLOT(onUpdateSettings()));
    connect(m_ui->ckbc_s6, SIGNAL(currentIndexChanged(int)),
            this, SLOT(onUpdateSettings()));
    connect(m_ui->ckbc_skew_s6, SIGNAL(currentIndexChanged(int)),
            this, SLOT(onUpdateSettings()));
    connect(m_ui->ckdt_s6, SIGNAL(currentIndexChanged(int)),
            this, SLOT(onUpdateSettings()));
    connect(m_ui->tpSkew, SIGNAL(currentIndexChanged(int)),
            this, SLOT(onUpdateSettings()));
    connect(m_ui->tpWidth, SIGNAL(currentIndexChanged(int)),
            this, SLOT(onUpdateSettings()));
    connect(m_ui->tpPolarity, SIGNAL(currentIndexChanged(int)),
            this, SLOT(onUpdateSettings()));

    connect(m_ui->ApplyAll, SIGNAL(clicked()),
            this, SLOT(onUpdateSettings()));

    connect(m_fecWindow->m_daqWindow->m_daq.m_fecs[m_fecIndex].m_fecConfigModule, SIGNAL(ReloadHybrid()),
            this, SLOT( onReloadSettings() ));

}

HybridWindow::~HybridWindow()
{
    delete m_ui;
}


void HybridWindow::on_Box_vmm1_clicked()
{
    if (m_ui->Box_vmm1->isChecked()){VMMBoxLogic(true,0);}
    else {VMMBoxLogic(false,0);}
}
void HybridWindow::on_Box_vmm2_clicked()
{
    if (m_ui->Box_vmm2->isChecked()){VMMBoxLogic(true,1);}
    else {VMMBoxLogic(false,1);}
}

void HybridWindow::VMMBoxLogic(bool checked, unsigned short vmm){
    unsigned short NotActiveBefore = 0;
    QList<QCheckBox*> a = m_ui->groupBox->findChildren<QCheckBox*>();
    for (unsigned short i = 0; i < a.size(); i++){
        if(i<vmm && !a.at(i)->isChecked()) NotActiveBefore++;
    }
    if (checked){
        m_ui->tabWidget->insertTab(vmm-NotActiveBefore, new VMMWindow(this,m_fecIndex,m_hybridIndex,vmm), QString(" VMM %0").arg(vmm+1));
        m_ui->tabWidget->setCurrentIndex(vmm-NotActiveBefore);
        m_fecWindow->m_daqWindow->m_daq.m_fecs[m_fecIndex].m_hybrids[m_hybridIndex].SetVMM(vmm, true);
    }
    else {
        m_ui->tabWidget->removeTab(vmm-NotActiveBefore);
        m_fecWindow->m_daqWindow->m_daq.m_fecs[m_fecIndex].m_hybrids[m_hybridIndex].SetVMM(vmm, false);
    }
}

void HybridWindow::UpdateWindow(){
    for (unsigned short m=0; m < VMMS_PER_HYBRID; m++){
        if (m_fecWindow->m_daqWindow->m_daq.m_fecs[m_fecIndex].m_hybrids[m_hybridIndex].GetVMM(m)){
            if (m == 0 && !m_ui->Box_vmm1->isChecked()){m_ui->Box_vmm1->setChecked(true); on_Box_vmm1_clicked();}
            if (m == 1 && !m_ui->Box_vmm2->isChecked()){m_ui->Box_vmm2->setChecked(true); on_Box_vmm2_clicked();}
        }
    }
}
void HybridWindow::LoadSettings(){

    while(!m_fecWindow->m_daqWindow->m_daq.CheckHybridPos(GetHybrid("axis"), GetHybrid("position"), m_fecIndex, m_hybridIndex )){
        SetHybrid("position", GetHybrid("position")+1);
    }

    m_ui->axis->setCurrentIndex(GetHybrid("axis"));
    m_ui->position->setValue(GetHybrid("position"));
    m_ui->ckbc_s6->setCurrentIndex(GetHybrid("CKBC"));
    m_ui->ckbc_skew_s6->setCurrentIndex(GetHybrid("CKBC_skew"));
    m_ui->ckdt_s6->setCurrentIndex(GetHybrid("CKDT"));
    m_ui->tpSkew->setCurrentIndex(GetHybrid("TP_skew"));
    m_ui->tpWidth->setCurrentIndex(GetHybrid("TP_width"));
    m_ui->tpPolarity->setCurrentIndex(GetHybrid("TP_pol"));
}

// ------------------------------------------------------------------------- //
void HybridWindow::onReloadSettings(){
    m_ui->ckbc_s6->setCurrentIndex(GetHybrid("CKBC"));
    m_ui->ckbc_skew_s6->setCurrentIndex(GetHybrid("CKBC_skew"));
    m_ui->ckdt_s6->setCurrentIndex(GetHybrid("CKDT"));
    m_ui->tpSkew->setCurrentIndex(GetHybrid("TP_skew"));
    m_ui->tpWidth->setCurrentIndex(GetHybrid("TP_width"));
    m_ui->tpPolarity->setCurrentIndex(GetHybrid("TP_pol"));
}
//


bool HybridWindow::SetHybrid(std::string feature, unsigned short value){
    if(m_fecWindow->m_daqWindow->m_daq.m_fecs[m_fecIndex].m_hybrids[m_hybridIndex].SetReg(feature, value)){
        return true;
    }
    else return false;
}
unsigned short HybridWindow::GetHybrid(std::string feature){
    return m_fecWindow->m_daqWindow->m_daq.m_fecs[m_fecIndex].m_hybrids[m_hybridIndex].GetReg(feature);
}

void HybridWindow::onUpdateSettings(){

    if(QObject::sender() == m_ui->axis){
        if(m_fecWindow->m_daqWindow->m_daq.CheckHybridPos( m_ui->axis->currentIndex() , GetHybrid("position"), m_fecIndex, m_hybridIndex )){
            SetHybrid("axis", m_ui->axis->currentIndex());
        }
        else{
            m_ui->axis->setCurrentIndex( GetHybrid("axis") );
            m_fecWindow->m_daqWindow->SetWarningMessage("Hybrid position occupied- resetted!", "orange");
        }


    }
    else if(QObject::sender() == m_ui->position){
        if(m_fecWindow->m_daqWindow->m_daq.CheckHybridPos(GetHybrid("axis"), m_ui->position->value() , m_fecIndex, m_hybridIndex )){
            SetHybrid("position", m_ui->position->value());
        }
        else{
            m_ui->position->setValue( GetHybrid("position") );
            m_fecWindow->m_daqWindow->SetWarningMessage("Hybrid position occupied- resetted!", "orange");
        }

    }
    else if(QObject::sender() == m_ui->ckbc_s6){
        SetHybrid("CKBC", m_ui->ckbc_s6->currentIndex());
    }
    else if(QObject::sender() == m_ui->ckbc_skew_s6){
        SetHybrid("CKBC_skew", m_ui->ckbc_skew_s6->currentIndex());
    }
    else if(QObject::sender() == m_ui->ckdt_s6){
        SetHybrid("CKDT", m_ui->ckdt_s6->currentIndex());
    }
    else if(QObject::sender() == m_ui->tpSkew){
        SetHybrid("TP_skew", m_ui->tpSkew->currentIndex());
    }
    else if(QObject::sender() == m_ui->tpWidth){
        SetHybrid("TP_width", m_ui->tpWidth->currentIndex());
    }
    else if(QObject::sender() == m_ui->tpPolarity){
        SetHybrid("TP_pol", m_ui->tpPolarity->currentIndex());
    }
    else if(QObject::sender() == m_ui->ApplyAll){
        for (unsigned short fec=0; fec < FECS_PER_DAQ; fec++){
            if (m_fecWindow->m_daqWindow->m_daq.GetFEC(fec) ){
                for (unsigned short hybrid=0; hybrid < HYBRIDS_PER_FEC; hybrid++){
                    if( m_fecWindow->m_daqWindow->m_daq.m_fecs[fec].GetHybrid(hybrid) ){

                        m_fecWindow->m_daqWindow->m_daq.m_fecs[fec].m_hybrids[hybrid].SetReg("CKBC", m_ui->ckbc_s6->currentIndex());
                        m_fecWindow->m_daqWindow->m_daq.m_fecs[fec].m_hybrids[hybrid].SetReg("CKBC_skew", m_ui->ckbc_skew_s6->currentIndex());
                        m_fecWindow->m_daqWindow->m_daq.m_fecs[fec].m_hybrids[hybrid].SetReg("CKDT", m_ui->ckdt_s6->currentIndex());
                        m_fecWindow->m_daqWindow->m_daq.m_fecs[fec].m_hybrids[hybrid].SetReg("TP_skew", m_ui->tpSkew->currentIndex());
                        m_fecWindow->m_daqWindow->m_daq.m_fecs[fec].m_hybrids[hybrid].SetReg("TP_width", m_ui->tpWidth->currentIndex());
                        m_fecWindow->m_daqWindow->m_daq.m_fecs[fec].m_hybrids[hybrid].SetReg("TP_pol", m_ui->tpPolarity->currentIndex());

                    }
                }
            }
        }
        m_fecWindow->m_daqWindow->m_daq.ApplyHybrids(m_fecIndex);
    }
}

void HybridWindow::on_pbReadI2C_pressed()
{
    QString result = m_fecWindow->m_daqWindow->m_daq.m_fecs[m_fecIndex].m_fecConfigModule->ReadI2C(m_hybridIndex, m_ui->cbChoiceI2C->currentIndex());
    m_ui->lineEditResultI2C->setText(result.toUpper());
}

