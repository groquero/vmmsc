#ifndef GLOBPARAMETER
#define GLOBPARAMETER

#define CONFIG_DIR "configs"

#define FECS_PER_DAQ 8 // max number, can be actived/deactivated
#define HYBRIDS_PER_FEC 8 // max number, can be actived/deactivated
#define VMMS_PER_HYBRID 2 // max number, can be actived/deactivated

#define VMM_CHANNELS 64

#endif // GLOBPARAMETER
